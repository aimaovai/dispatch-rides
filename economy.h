#ifndef ECONOMY_H
#define ECONOMY_H

#include <iostream>
#include <string>
#include "driver.h"

using namespace std;

class economy : public driver
{
	private:
		int passseat;// 2 or less passengers
		int cargocap;
	//	int passengers;
		
	public:
		economy();
		~economy();
		economy(int seat, int cargo, string name, string id, float rate, char avail, char pet, bool hand, string note);
	//	void SetPassengers(int num);
		int GetSeats();//return number of passenger seats available for type of car
		//void SetCargocap();
		int GetCargo();	
		void PrintInfo();	
};

#endif
