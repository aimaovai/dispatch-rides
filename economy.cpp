#include <iostream>
#include <string>
#include "driver.h"
#include "economy.h"

using namespace std;

economy::economy()
{
	passseat = 2;
	cargocap = 2;
}

economy::economy(int seat, int cargo, string name, string id, float rate, char avail, char pet, bool hand, string note):driver(name,id,rate,avail,pet,hand,note)
{
	passseat = seat;
	cargocap = cargo;
}

int economy::GetSeats()
{
	return passseat;
}

int economy::GetCargo()
{
	return cargocap;
}

void economy::PrintInfo()
{
	driver::PrintInfo();
	cout << "Ride type: Economy" << endl;
	cout << "Max number of passengers: " << passseat << endl;
	cout << "Max ammount of luggage: " << cargocap << endl;
	cout << endl;
}


