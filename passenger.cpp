#include <iostream>
#include <string>
#include "passenger.h"
#include <time.h>

using namespace std;

passenger::passenger()//default constructor
{
	name = "empty";
	passID = "empty";
	paytype = 'n';
	handicap = false;
	def_rating = 0.0;
	petsowned = false;
	timemins = 0;
	timehrs = 0;
	date = 0;
	month = 0;
	year = 0;
}
passenger::passenger(string passengername, string ID, char payment, bool handic, bool ownpet, float rate)
{
	name = passengername;
	passID = ID;
	switch(payment)
	{
		case 'c':
			paytype = 'c';
			break;
		case 'd':
			paytype = 'd';
			break;
		case 'e':
			paytype = 'e';
			break;
		default:
			break;
	}
	handicap = handic;
	petsowned = ownpet;
	def_rating = rate;
}
void passenger::SetPassengerID(string newID)
{
	passID = newID;
}
string passenger::GetPassengerID()
{
	return passID;
}
void passenger::SetPassengerName(string name)
{
	this->name = name;
}
string passenger::GetPassengerName()
{
	return name;
}
void passenger::SetPayment(char pay)
{
	switch(pay)
	{
		case 'c':
			paytype = 'c';
			break;
		case 'd':
			paytype = 'c';
			break;
		case 'e':
			paytype = 'e';
			break;
		default: 
			break;
	}
}
char passenger::GetPayment()//fixme
{
	if(paytype=='c')
	{
		cout << "Credit";
	}
	else if(paytype=='d')
	{
		cout << "Debit";
	}
	else
	{
		cout << "Cash";
	}
	return paytype;
}
void passenger::SetHandicap(bool handicapped)
{
	handicap = handicapped;
}
bool passenger::GetHandicap()
{
	if(handicap==true)
	{
		cout << "Yes";
	}
	else
	{
		cout << "No";
	}
	return handicap;
}
void passenger::SetDefaultRating(float rating)
{
	def_rating = rating;
}
float passenger::GetDefaultRating()
{
	return def_rating;
}
void passenger::SetPetOwned(bool pets)
{
	petsowned = pets;
}
bool passenger::GetPetOwned()
{
	if(petsowned==true)
	{
		cout << "Yes";
	}
	else
	{
		cout << "No";
	}
	return petsowned;
}
void passenger::SetTime(int hour, int minutes, int day, int monthnum, int years)
{
	timehrs = hour;
	timemins = minutes;
	date = day;
	month = monthnum;
	year = years;
}
void passenger::GetTime()
{
	struct tm *usertime;

	time_t currtime;
	time( &currtime);
	usertime = gmtime( &currtime);
	usertime->tm_year = year - 1900;
	usertime->tm_mon = month - 1;
	usertime->tm_mday = date;
	usertime->tm_hour = timehrs;
	usertime->tm_min = timemins;
	usertime->tm_sec = 0;

	cout << "Your set ride time is: " << asctime(usertime);
}
