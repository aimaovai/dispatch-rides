#include <iostream>
#include <string>
#include "driver.h"
#include "group.h"

using namespace std;

group::group()
{
	numseats = 0;
	numcargo = 0;
}

group::group(int seat, int cargo, string name, string id, float rate, char avail, char pet, bool hand, string note):driver(name,id,rate,avail,pet,hand,note)
{
	numseats = 7;
	numcargo = 4;
}

int group::GetSeats()
{
	return numseats;
}

int group::GetCargo()
{
	return numcargo;
}

void group::PrintInfo()
{
	driver::PrintInfo();
	cout << "Ride type: Group" << endl;
	cout << "Max number of passengers: " << numseats << endl;
	cout << "MAx amount of luggage: " << numcargo << endl;
	cout << endl;
}
