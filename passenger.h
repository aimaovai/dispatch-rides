#ifndef PASSENGER_H
#define PASSENGER_H

#include <iostream>
#include <string>
using namespace std;

class passenger
{
	private:
		string name;
		string passID;
		enum payment{credit='c', debit='d', cash='e'};//enum for payment preference
		char paytype;
		bool handicap;
		float def_rating;
		bool petsowned;
		int timemins;
		int timehrs;
		int date;
		int month;
		int year;

	public:
		passenger();
		passenger(string passengername, string ID, char payment, bool handic, bool ownpet, float rate);
		passenger* nextpassenger();
		~passenger(){};
		void setnextpassenger(passenger* pntr);
		void SetPassengerID(string pID);
		string GetPassengerID();
		void SetPassengerName(string pname);
		string  GetPassengerName();
		void SetPayment(char pay);
		char GetPayment();
		void SetHandicap(bool phandicap);
		bool GetHandicap();
		void SetDefaultRating(float pdrating);
		float GetDefaultRating();
		void SetPetOwned(bool ppet);
		bool GetPetOwned();
		void SetTime(int hour, int minutes, int day, int monthnum, int years);
		void GetTime();
};

#endif
