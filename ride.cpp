#include <iostream>
#include <string>
#include <time.h>
#include "ride.h"

using namespace std;

ride::ride()
{
	rideID = "none";
	date = 0;
	month = 0;
	year = 0;
	picktimemins = 0;
	picktimehrs = 0;
	droptimemins = 0;
	droptimehrs = 0;
	statusofride = 'A';
	driver = "none";
	vehicle = 'n';
	passenger = "none";
	droplocation = "none";
	picklocation = "none";
	numpass = 0;
	petsallowed = false;
	custrating = 0.00;
	
}

ride::ride (string ID, string driver, string passenger, char car, int date, int month, int year, int picktimemins, int picktimehrs, char statusofride, string droplocation, string picklocation, int numpass, bool petsallowed, float custrating)
{
	rideID = ID;
	this->date = date;
	this->month = month;
	this->year = year;
	this->picktimemins = picktimemins;
	this->picktimehrs = picktimehrs;
	this->statusofride = statusofride;
	this->driver = driver;
	vehicle = car;
	this->passenger = passenger;
	this->droplocation = droplocation;
	this->picklocation = picklocation;
	this->numpass = numpass;
	this->petsallowed = petsallowed;
	this->custrating = custrating;
}

ride::ride (string ID, string driver,string passenger, char car, int date, int month, int year, int picktimemins, int picktimehrs, int droptimemins, int droptimehrs, char statusofride, string droplocation, string picklocation, int numpass, bool petsallowed, float custrating)
{
	rideID = ID;
	this->date = date;
	this->month = month;
	this->year = year;
	this->picktimemins = picktimemins;
	this->picktimehrs = picktimehrs;
	this->droptimemins = droptimemins;
	this->droptimehrs = droptimehrs;
	this->statusofride = statusofride;
	this->driver = driver;
	vehicle = car;
	this->passenger = passenger;
	this->droplocation = droplocation;
	this->picklocation = picklocation;
	this->numpass = numpass;
	this->petsallowed = petsallowed;
	this->custrating = custrating;
}

void ride::SetID(string ID)
{
	rideID = ID;
}

string ride::GetID()
{
	return rideID;
}

void ride::SetDriver(string drivertoset)
{
	driver = drivertoset;
}

string ride::GetDriver()
{
	return driver;
}

void ride::SetPassenger(string passengertoset)
{
	passenger =  passengertoset;
}

string ride::GetPassenger()
{
	return passenger;
}

void ride::SetPickLoc(string picloc)
{
	picklocation = picloc;

}

string ride::GetPickLoc()
{
	return picklocation;
}

void ride::SetPets(bool pets)
{
	petsallowed = pets;
}

bool ride::GetPets()
{
	if(petsallowed==true)
	{
		cout << "Yes";
	}
	else
	{
		cout << "No";
	}
	return petsallowed;
}

void ride::SetPickTime(int ptimehrs, int ptimemins, int ptimedate, int ptimemonth, int ptimeyr)
{
	picktimehrs = ptimehrs;
	picktimemins = ptimemins;
	date = ptimedate;
	month = ptimemonth;
	year = ptimeyr;
}

int ride::GetPickTime()
{
	struct tm *picktime;
	time_t currtime, start;
	time( &currtime);
	picktime = localtime( &currtime);
	picktime->tm_year = year - 1900;
	picktime->tm_mon = month - 1;
	picktime->tm_mday = date;
	picktime->tm_hour = picktimehrs;
	picktime->tm_min = picktimemins;
	picktime->tm_sec = 0;

	start = mktime(picktime);
	cout << asctime(picktime);
	return start;
}

void ride::SetDropTime(int dtimehrs, int dtimemins)
{
	droptimehrs = dtimehrs;
	droptimemins = dtimemins;	
}
int ride::GetDropTime()
{
	struct tm *droptime;
	time_t currtime, endtime;
	time( &currtime);
	droptime = localtime( &currtime);
	droptime->tm_year = year - 1900;
	droptime->tm_mon = month - 1;
	droptime->tm_mday = date;
	droptime->tm_hour = droptimehrs;
	droptime->tm_min = droptimemins;
	droptime->tm_sec = 0;

	endtime = mktime(droptime);
	cout <<  asctime(droptime);
	return endtime;

}

int ride::GetMins()
{
	return picktimemins;
}

int ride::GetHrs()
{
	return picktimehrs;
}

int ride::GetDate()
{
	return date;
}

int ride::GetMonth()
{
	return month;
}

int ride::GetYear()
{
	return year;
}

void ride::SetDropLoc(string location)
{
	droplocation = location;
}

string ride::GetDropLoc()
{
	return droplocation;
}

void ride::SetNumPassengers(int numpass)
{
	this->numpass = numpass;
}

int ride::GetNumPassengers()
{
	return numpass;
}

void ride::SetStatus(char stats)
{
	switch(stats)
	{
		case 'C' :
			statusofride = 'C';
			break;
		case 'A':
			statusofride = 'A';
			break;
		case 'c':
			statusofride = 'c';
			break;
		default:
			break;
	}
}

void ride::Setvehicle(char vehicle)
{
	this->vehicle = vehicle;
}

char ride::Getvehicle()
{
	if(vehicle=='C')
	{
		cout << "Compact";
	}
	else if(vehicle=='S')
	{
		cout << "Sedan";
	}
	else if(vehicle=='U')
	{
		cout << "SUV";
	}
	else if(vehicle=='V')
	{
		cout << "Van";
	}
	else
	{
		cout << "Other";
	}
	
	return vehicle;
}
char ride::GetStatus()
{
	if(statusofride == 'C')
	{
		cout << "Ride complete.";
	}
	else if(statusofride == 'A')
	{
		cout << "Ride active.";
	}
	else
	{
		cout << "Ride cancelled";
	}
	return statusofride;
}

void ride::SetCustRating(float rate)
{
	custrating = rate;
}

float ride::GetCustRating()
{
	return custrating;
}



