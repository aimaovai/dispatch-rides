#ifndef RIDES_H
#define RIDES_H

#include <iostream>
#include <string>
#include "ride.h"
#include "driver.h"
#include "passenger.h"
#include "drivers2.h"
#include "passengers.h"

using namespace std;

class rides
{
	private:
		int rcounter;
		typedef vector<ride*>ridetype;
		ridetype ridelist;
	public:
		rides(){rcounter = 0;}
		~rides(){}
		void addride();
		void adddrivertoride();
		void addpassengertoride();
		void editride(string id);
		void deleteride(char stat);//based oin complete or cancelled status
		void deleteride(string id);
		void findridebyID(string rID);
		void printbyID(string id);
		void updateride();
		time_t gettime(int year, int month, int date, int picktimemins, int picktimehrs);
		time_t getridetime(string rideid);
		string finddriverbyid(string id);
		void findridebydriver(string driver);
		char findstatus(char status);
		void findridebypassenger(string passenger);
		void findridebystatus(char rstatus);//after doing a find 
		void printrideatpoint(int point); 
		void printallrides();
		void storerides();
		void loadrides();
		void clean();
};
#endif
