#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <stdlib.h>
#include <iomanip>
#include "passengers.h"
#include "passenger.h"

using namespace std;



void passengers::addpassenger()
{
	int i=0; 
	string newid, hold, name, pid;
	float rate;
	size_t dec;
	char needhandi, ownpet, payment;
	bool handistat, petstat, same=true;
	passenger *holder;
		cout << "Enter passenger name: ";
		getline(cin,name);
		cout << "Enter passenger ID: ";
		cin >> pid;
		cin.ignore();
		
		while(same)
		{
			for(int a=0; a<passengerlist.size(); a++)
			{
				if(passengerlist.at(a)->GetPassengerID()!=pid)//check if entered ID already belongs to previous passengeir
				{
					same = false;
					passengerlist.at(i)->SetPassengerID(pid);//assign new ID to passsneger
				}
				else
				{
					cout << "Passenger ID already exists. Please enter a new ID or press 1 to exit to main menu: " << endl;
					cin >> pid;
					cin.ignore();
					if(pid=="1")//cehck if user wants to exit
					{
						system("clear");
						break;
					}							
				}
			}
		}
		
		cout << "Enter payment type('c' for credit, 'd' for debit, 'e' for cash): ";
		cin >> payment;
		cin.ignore();
		while((payment!='c')&&(payment!='d')&&(payment!='e'))
		{
			cout << endl;
			cout << "Please enter proper selection. 'c' for credit, 'd' for debit, 'e' for cash: ";
			cin >> payment;
			cin.ignore();
		}
		cout << "Do you need handicap availabilty? (Y/y for yes or N/n for no)";
		cin >> needhandi;
		cin.ignore();
		while((needhandi!='Y')&&(needhandi!='y')&&(needhandi!='N')&&(needhandi!='n'))
		{
			cout << endl;
			cout << "Please enter a valid option. Y/y for yes, N/n for no: ";
			cin >> needhandi;
			cin.ignore();
		}
		if((needhandi=='Y')||(needhandi=='y'))
		{
			handistat = true;
		}
		else
		{
			handistat = false;
		}
		cout << "Do you have pets that you want to take on the ride? (Y/y for yes or N/n for no): ";
		cin >> ownpet;
		cin.ignore();
		while((ownpet!='Y')&&(ownpet!='y')&&(ownpet!='N')&&(ownpet!='n'))
		{
			cout << endl;
			cout << "Please enter a valid selection. Y/y for yes or N/n for no: ";
			cin >> ownpet;
			cin.ignore();
		}
		if((ownpet=='Y')||(ownpet=='y'))
		{
			petstat = true;
		}
		else
		{
			petstat = false;
		}
		cout << "Enter default minimum rating for driver/ride filters: ";
		cin >> rate;
		cin.ignore();
		holder = new passenger(name, pid, payment, handistat, petstat, rate);
		passengerlist.push_back(holder);
		counter++;
system("clear");
}

void passengers::editpassenger(string id)
{
	passenger *edit;
	float rate;
	char hand, pet;
	string name, quit;
	int select;
	string passid;
	bool notfound = true, same = true, setpet, sethandi;
//	while(passid!="0")
//	{
		cout << "Enter passenger ID or enter q to return to main menu: " << endl;
		cin >> passid;
		cin.ignore();
		while(passid=="q")
		{
			break;
		}
//		while(notfound)
//		{
			for(int i=0; i<passengerlist.size(); i++)
			{
				if(passengerlist.at(i)->GetPassengerID()==passid)//check if user exists in list o passengers
				{
					notfound = false;
					
					cout << "Passenger found." << endl;
					cout << endl;
					printallpassengers();
					cout << endl;
					cout << "What information would you like to change about this passenger?\n1. Name\n2. Handicap requirements\n3. Pet restrictions\n4. Minimum required driver rating\nPlease select 1, 2, or 3, or press 0 to return to the main menu." << endl;
					cin >> select;
					cin.ignore();
					while((select!=1)&&(select!=2)&&(select!=3)&&((select!=4)&&select!=0))//check for choice validity
					{
						cout << "Please select either 1, 2, 3, or 4." << endl;
						cin >> select;
						cin.ignore();
					}
					switch(select)
					{
						case 0:
							system("clear");
							break;
						case 1:
							cout << "Enter new name: " << endl;
							cin >> name;
							cin.ignore();
							passengerlist.at(i)->SetPassengerName(name);//set passenger name to new name
							system("clear");
							break;
		
						case 2:
							cout << "Enter handicap requirement (Y/y for yes or N/n for no: " << endl;
							cin >> hand;
							cin.ignore();
							while((hand!='Y')&&(hand!='y')&&(hand!='N')&&(hand!='n'))//check for answer validity
							{
								cout << "Please enter Y/y or N/n." << endl;
								cin >> hand;
								cin.ignore();
							}
							if((hand=='y')||(hand=='Y'))
							{
								sethandi = true;
							}
							else
							{
								sethandi = false;
							}
							passengerlist.at(i)->SetHandicap(sethandi);//set handicap requirement to new option
							system("clear");
							break;
						case 3:
							cout << "Enter new pet requirement. (Y/y for yes, or N/n for no): " << endl;
							cin >> pet;
							cin.ignore();
							while((pet!='Y')&&(pet!='y')&&(pet!='N')&&(pet!='n'))//check for answer validity
							{
								cout << "Please enter Y/y or N/n." << endl;
								cin >> pet;
								cin.ignore();
							}
							if((pet=='y')||(pet=='Y'))
							{
								setpet = true;
							}
							else
							{
								setpet = false;
							}
							passengerlist.at(i)->SetPetOwned(setpet);//set pet choice to new choice
							system("clear");
							break;
						case 4:
							cout << "Enter new rating: " ;
							cin >> rate;
							cin.ignore();
							passengerlist.at(i)->SetDefaultRating(rate);
							break;
						default:
							break;
					}
				}
			}
		//}
			if(notfound)
			{
				string key;
				cout << "Passenger not found. Press any key to return to main menu." << endl;
				cin >> key;
				system("clear");
			}
//	}
}

void passengers::deletepassengerbyID(string ID)
{
	passenger* ndelptr;
	for(int i=0; i<passengerlist.size(); i++)
	{
		if(passengerlist.at(i)->GetPassengerID()==ID)
		{
			delete passengerlist[i];
			passengerlist.erase(passengerlist.begin()+i);
			counter = passengerlist.size();
		}
		
	}
	return;

}

int passengers::findpassengerbyID(string id)
{
		int index;
	for(index =0; index<passengerlist.size(); index++)
	{
		if(passengerlist.at(index)->GetPassengerID()==id)
		{
			return index;
		}
	}
return -1;	
}

passenger* passengers::findpassenger(string id)
{

	for (int i =0; i < counter; i++)
	{
        if (passengerlist[i]->GetPassengerID() == id) 
		{
			return passengerlist[i];
		}
    }
  
    return NULL;
}

string passengers::getpassenger(string id)
{	
	for(int i=0; i<passengerlist.size(); i++)
	{
		if(passengerlist.at(i)->GetPassengerID()==id)
		{
			return id;
		}
	}
	
	return "none";
}

bool passengers::findpet(string id, int index)
{
		return passengerlist.at(index)->GetPetOwned();
}

bool passengers::findhandi(string id, int index)
{
 	return passengerlist.at(index)->GetHandicap();
}

float passengers::findrating(string id, int index)
{			
	return passengerlist.at(index)->GetDefaultRating();
}


void passengers::printallpassengers()
{
	cout << "Passengers:" << endl;
	cout << left;
	cout << setw(20);
	cout << "Name:";
	cout << setw(15);
	cout << "ID:";
	cout << setw(15);
	cout << "Payment:";
	cout << setw(15);
	cout << "Handicap:";
	cout << setw(15);
	cout << "Owns pet:";
	cout << endl;
	passenger *ptr;
	int i;
	for(auto it = passengerlist.begin(); it!=passengerlist.end(); ++it)
	{
		ptr = *it;
		cout << left;
		cout << setw(20);
		cout << ptr->GetPassengerName();
		cout << setw(15);
		cout << ptr->GetPassengerID();
		cout << setw(15);
		ptr->GetPayment();
		cout << setw(15);
		ptr->GetHandicap();
		cout << setw(15);
		ptr->GetPetOwned();
		cout << endl;
	}
}

void passengers::printfoundpassengerbyID(string ID, passengers *pptr1)
{
	int index;
	index=pptr1->findpassengerbyID(ID);
	if(index==-1)
	{
		cout << "Passenger not found or does not exist." << endl;
		return;
	}
	else
	{
	//	for(int i=0; i<counter; i++)
	//	{
	//		if(passengerlist.at(i)->GetPassengerID()==index)
	//		{
				cout << "Name: " << passengerlist.at(index)->GetPassengerName() << endl;
				cout << "ID: " << passengerlist.at(index)->GetPassengerID() << endl;
				cout << "Payment method: " << passengerlist.at(index)->GetPayment() << endl;
				cout << "Pets owned by passenger: " << passengerlist.at(index)->GetPetOwned() << endl;
	//			cout << endl;
	//		}
	//	}
	}
}

void passengers::storepassenger()
{
	ofstream passengerout;
	passengerout.open("passengers.data");
	passengerout << counter << endl;
	passenger *newholder;
	for(auto it=passengerlist.begin(); it!=passengerlist.end(); ++it)
	{
		newholder = *it;
		passengerout << newholder->GetPassengerName() << ",\t";
		passengerout << newholder->GetPassengerID() << "\t";
		passengerout << newholder->GetPayment() << "\t";
	    passengerout << newholder->GetHandicap() << "\t";
		passengerout << newholder->GetPetOwned() << "\t" << newholder->GetDefaultRating() <<  endl;
	}
	passengerout.close();
}
void passengers::loadpassenger()
{
	string pid;
	float rate;
	string pname;
	char payment; 
	bool pets, handicap;
	ifstream passengerin;
	passengerin.open("passengers.data");
	passengerin >> counter;
	passengerin.ignore();

	for(int i=0; i<counter; i++)
	{	
		getline(passengerin, pname,',');
		passengerin >> pid >> payment >> handicap >> pets >> rate;
		passengerlist.push_back(new passenger(pname, pid, payment, handicap, pets, rate));
		
	}
	passengerin.close();
}
void passengers::clearspace()
{
	for(auto it = passengerlist.begin(); it != passengerlist.end(); ++it)
	{
		delete *it;
	}
	passengerlist.clear();
}
