#ifndef GROUP_H
#define GROUP_H

#include<iostream>
#include <string>
#include "driver.h"

using namespace std;

class group : public driver
{
	protected:
		int numseats;//5-7 passengers
		int numcargo;
	public:
		group();
		~group();
		group(int seat, int cargo, string name, string id, float rate, char avail, char pet, bool hand, string note);
		int GetSeats();
		int GetCargo();
		void PrintInfo();
};

#endif
