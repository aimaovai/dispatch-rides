#ifndef DRIVERS_H
#define DRIVERS_H

#include <iostream>
#include <vector>
#include <string>
#include "driver.h"
#define CAPSIZE 3
class drivers
{
	private:
		int count;
		int maxcount;
		vector<driver*>driverlist;
		//drivertype driverlist;
	public:
		drivers(){count=0;}
		~drivers(){}
		void adddriver();
		void editdriver(string id);
		int Getdrcounter(){return count;}
		void deletedriverbyID(string ID);
		void GetDrivers(int passengerloc);
		int findbyname(string name);
		driver* findbyID(string idnum);
		bool findbyid(string id);
		string findbypoint(int point);
		char getcartype(int point);
		bool checkpassengercap(int num, int index);
		bool checkdriveravai(int index);
		bool checkdriverrating(int index, float rate);
		void printdriver(int index);
		string getdriver(int point);
		void checkrating(float rating);
		bool GetPetofdriver(int index, bool pet);
		bool gethandistatus(int index, bool handi);
		void printalldrivers();
		void storedrivers();
		void loaddrivers();
		void clear();
};

#endif
