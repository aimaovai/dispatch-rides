#ifndef LUXURY_H
#define LUXURY_H

#include <iostream>
#include <string>
#include "driver.h"

using namespace std;

class luxury : public driver
{
	protected:
		int cargoamount;//
		int seatamount;//limo style or lamborghini, or covert
		string notes;
	public:
		luxury();
		~luxury();
		luxury(int seat, int cargo, string notes, string name, string id, float rate, char avail, char pet, bool hand, string note);
		int GetCargo();
		int GetSeats();
		void SetNotes(string note);
		string GetNotes();//
		void PrintInfo();
};

#endif
