#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <stdlib.h>
#include <iomanip>
#include <stdio.h>
#include "driver.h"
#include "passenger.h"
#include "passengers.h"
#include "drivers2.h"
#include "ride.h"
#include "rides.h"
using namespace std;
extern passengers riders;
extern drivers ridedrivers;
extern rides dispatch;
//using namespace std;

void rides::addride()
{
	int date, month, year, picktimemins, picktimehrs, droptimemins, droptimehrs, num, point, i=0;
	int index;
	char statusofride, cartype;
	string driverID, passengerID, rideID, droploc, pickloc, rideid;
	float rating, rate=0.00;
	size_t dec;
	bool pets, same = true, sametime=true, handicap, found = false;
	ride *ridehold;	
	cout << "Enter new 8 digit ride ID: ";
	cin >> rideid;
	cin.ignore();
	rideID = stoi(rideid, &dec);
		for(int a=0; a<ridelist.size(); a++)
		{
			if(ridelist.at(a)->GetID()!=rideID)//check if entered ID already belongs to ride
			{
				same = false;
				ridelist.at(i)->SetID(rideID);
			}
			else
			{
				cout << "Ride with ID already exists. Enter new ID: ";
				cin >> rideid;
				cin.ignore();
				rideID = stoi(rideid, &dec);
				if(rideID=="1")
				{
					system("clear");
					break;
				}
			}
		}			
	
	riders.printallpassengers();
	cout << "Enter passenger ID: ";
	cin >> passengerID;
	cin.ignore();
	cout << endl;
	index = riders.findpassengerbyID(passengerID);//returns the index of the passenger in passengerlis

	cout << "Enter the number of passengers for the ride: "; 
	cin >> num;
	cin.ignore();
	
	handicap = riders.findhandi(passengerID, index);//returns the handicap status of the passenger at index of passengerlist
	pets = riders.findpet(passengerID, index);//returns the pet reuirement of passenger at index
	rating = riders.findrating(passengerID, index);//returns the rating requirement of passenger at index
	cout << pets << " " << rating << " " << handicap << endl;//delete me
	int drivercount = ridedrivers.Getdrcounter();//get number of drivers in driverlist
	cout << drivercount << endl;//delete me
	for(int i=0; i<drivercount; i++)//iterate through to find driver with matching properties
	{
		if(ridedrivers.checkdriveravai(i)==true)//checkk if driver is available
		{
			cout << ridedrivers.checkdriveravai(i) << endl;
			if(ridedrivers.checkdriverrating(i, rating)==true)//check if driver rating is higher than specified by passenger
			{ cout << ridedrivers.checkdriverrating(i, rating) << " ";

				if(ridedrivers.gethandistatus(i, handicap)==true)//check if driver provides handicap accessibility
				{
					cout << ridedrivers.gethandistatus(i, handicap) << " ";
					if(ridedrivers.GetPetofdriver(i, pets)==true)//check if driver allows pets
					{
						cout << ridedrivers.GetPetofdriver(i, pets) << endl;
							found == true;
							cout << "found driver" << endl;//delete me
							point = i;
							driverID = ridedrivers.getdriver(point);
							break;
						
					}
				}
			}
		}
	}
	if(found==false)
	{
		cout << "Driver with matching requirements not found" << endl;
		cout << endl;
	}
	
	if(found ==true)
	{
	 cout << "Enter the day, month, and year digits in following order: dd mm yyyy" << endl;
        cin >> date >> month >> year;
        cin.ignore();

        cout << "Enter the pick up time in the following order: hours mins " << endl;
        cin >> picktimehrs >> picktimemins;
        cin.ignore();

        time_t ridetime = gettime(year, month, date, picktimemins, picktimehrs);//returns time value as entered by the passenger


	while(sametime)//check while the time is the same as previous ride
	{
		for(int a=0; a<ridelist.size(); a++)//iterate through to check ride times
		{
			if(dispatch.finddriverbyid(driverID)!=driverID)
			{
				if(dispatch.getridetime(rideID)!=ridetime)
				{
					sametime = false;
				}
			}
			else
			{
				cout << "Driver is unavailable at that time. Please enter a different time in the following order: hours mins " << endl;
				cin >> picktimehrs >> picktimemins;
				cin.ignore();
				ridetime = gettime(year, month, date, picktimemins, picktimehrs);
			}
		}
	}

	
	statusofride = 'A';//set ride status to active
	
	
	cout << "Enter the pick up location: ";
	getline(cin, pickloc);
	cout << "Enter the destination: ";
	getline(cin, droploc);
	
	ridehold = new ride(rideID, driverID, passengerID, cartype, date, month, year, picktimemins, picktimehrs,  statusofride, droploc, pickloc, num, pets, rate);//creates new ride entity
	ridelist.push_back(ridehold);//insert into vector
	rcounter++;//increase counter after adding ride
	}
}

void rides::editride(string id)
{
	string driver, passenger; 
	int date, month, year, picktimemins, picktimehrs, droptimemins, droptimehrs, num, point, i, choice, ridetime;
	char statusofride, pet, needhandi;
	string droploc, pickloc, rideID;
	float rating, rate;
	bool found = false;
	ride *cptr;
	while(choice!=0)
	{
			for(i=0; i<ridelist.size(); i++)
			{
				if(cptr->GetID()==id)//check if ride exists
				{
					bool pets, same = true, sametime=true, passcap=true, found=false, handicap;
					cout << "Choose an action:\n0. Exit to main menu\n1. Update ride status\n2. Update pick up address\n3. Update drop off address\n4. Update pick up time\n5. Update passenger count\n" << endl;
					cin >> choice;
					switch(choice)
					{
						case 1:
							cout << "Enter new status for ride: ";
							cin >> statusofride;
							cptr->SetStatus(statusofride);//update the ride status
							break;
							
						case 2:
							cout << "Enter the day, month, and year digits in following order: dd mm yyyy" << endl;
							cin >> date >> month >> year;
							cin.ignore();
							cout << "Enter the pick up time in the following order: hours mins " << endl;
							cin >> picktimehrs >> picktimemins;
							cin.ignore();
							ridetime = gettime(year, month, date, picktimemins, picktimehrs);//get time value of new time
							while(sametime)
							{
								if(dispatch.getridetime(rideID)!=ridetime)//check if time clashes with other ride
								{
									sametime = false;
								}
								else
								{
									cout << "Driver is unavailable at that time. Please enter a different time in the following order: hours mins " << endl;
									cin >> picktimehrs >> picktimemins;
									cin.ignore();
									ridetime = gettime(year, month, date, picktimemins, picktimehrs);
								}
							}
							break;
							
						case 3:
							cout << "Enter new pick up address: ";
							getline(cin, pickloc);
							cptr->SetPickLoc(pickloc);//set new pickup location
							break;
						
						case 4:
							cout << "Enter new drop off address: ";
							getline(cin, droploc);
							cptr->SetDropLoc(droploc);//set new drop location
							break; 
							
						case 5:
							cout << "Enter new passenger count: ";
							cin >> num;
							cin.ignore();
				/*			if(ridedrivers.checkpassengercap(i, num)==true)
							{
								cptr->SetNumPassengers(num);//set new passenger amount
							}
							else
							{
								cout << "Number of passengers exceeds ride capacity. Please create a new ride to contain that amount of passengers.\nNow exiting to main menu.\n";
							}*/
							break;
						
						default:
							break;
							
					}
				}
			}
	}
}

time_t rides::getridetime(string rideid)
{
	ride *ptr;
	for(int i=0; i<ridelist.size(); i++)
	{
		if(ptr->GetID()==rideid)
		{
			return ridelist.at(i)->GetPickTime();//get time entered by passenger in time value
		}
	}
	
	return 0;
}

time_t rides::gettime(int year, int month, int date, int picktimemins, int picktimehrs)
{
	struct tm *usertime;
	time_t nowtime;
	time( &nowtime);
	usertime = localtime( &nowtime);
	usertime->tm_year = year-1900;
	usertime->tm_mon = month-1;
	usertime->tm_mday = date;
	usertime->tm_hour = picktimehrs;
	usertime->tm_min = picktimemins;
	usertime->tm_sec = 0;
	time_t pick = mktime(usertime);
	
	return pick;	
}

void rides::deleteride(char stat)
{
	ride* deleteptr;
	for(int i=0; i<ridelist.size(); i++)
	{
		if(deleteptr->GetStatus() ==stat)
		{
			delete ridelist[i];//delete ride entity
			ridelist.erase(ridelist.begin()+i);//delete the entity at point in vector
			rcounter = ridelist.size();//decrease number of rides
		}
	}
	return;
}

void rides::deleteride(string id)
{
	ride *delptr;
	for(int i=0; i<ridelist.size(); i++)
	{
		if(delptr->GetID()==id)
		{
			delete ridelist[i];
			ridelist.erase(ridelist.begin()+i);
			rcounter =  ridelist.size();
		}
	}
}

string rides::finddriverbyid(string id)
{
	ride *ptr;
	for(auto it = ridelist.begin(); it !=ridelist.end(); ++it)
	{
		ptr = *it;
//		if(ridedrivers.findbyID(ptr->GetDriver()==id))
		if(ridedrivers.findbyid(id)==true)
		{
			return ptr->GetDriver();// return driver id
		}
	}
}


void rides::findridebyID(string rID)
{
	int index = 0;
	for(index =0; ridelist.at(index)->GetID()!=rID; index++)
	{
		if(index==rcounter)
		{
			return;
		}
		else
		{
			cout << ridelist.at(index);//print the ride with corresponding id number

		}
	}
}

void rides::printbyID(string id)
{
	ride *rpt;
	for(int i=0; i<ridelist.size(); i++)
	{
		if(rpt->GetID()==id)
		{
			cout << ridelist.at(i);//print ride
		}
	}
}


void rides::findridebydriver(string driver)
{
	bool found=false;
	ride *prtride;
	for(int i=0; i<ridelist.size(); i++)
	{
		if(ridelist.at(i)->GetDriver()==driver)
		{
			printrideatpoint(i);
		}
	}

}
void rides::findridebypassenger(string passenger)
{
	for(int i=0; ridelist.size(); i++)
	{
		if(ridelist.at(i)->GetPassenger()==passenger)
		{
			cout << ridelist.at(i);//find and print ride(s) with amtching passenger id
		}
	}
}
void rides::findridebystatus(char rstatus)
{
	ride *pointer;
	for(int i=0; i<ridelist.size(); i++)
	{
		if(pointer->GetStatus()==rstatus)//check if ride status matches input status
		{
			cout << ridelist.at(i) << endl;
		}
	}
}

void rides::printrideatpoint(int point)
{
	ride *print;
	cout << "Rides:" << endl;
	cout << left;
	cout << setw(25);
	cout << "ID:";
	cout << setw(20);
	cout << "Passenger:";
	cout << setw(20);
	cout << "Driver:";
	cout << setw(20);
	cout << "Vehicle:";
	cout << setw(20);
	cout << "Pets:";
	cout << setw(20);
	cout << "Time:";
	cout << setw(20);
	cout << "Status:";
	cout << endl;
	
	cout << left;
	cout << setw(25);
	cout << ridelist.at(point)->GetID();
	cout << setw(20);
	cout << ridelist.at(point)->GetPassenger();
	cout << setw(20);
	ridelist.at(point)->GetDriver();
	cout << setw(20);
	ridelist.at(point)->Getvehicle();
	cout << setw(20);
	ridelist.at(point)->GetPets();
	cout << setw(20);
	cout << ridelist.at(point)->GetNumPassengers();
	cout << setw(20);
	cout << ridelist.at(point)->GetPickTime();
	cout << setw(20);
	cout << ridelist.at(point)->GetStatus();
	cout << endl;
	cout << endl;
}

void rides::printallrides()
{
	cout << "Rides:" << endl;
	cout << left;
	cout << setw(20);
	cout << "Ride ID:";
	cout << setw(20);
	cout << "Passenger ID:";
	cout << setw(20);
	cout << "Driver ID:";
	cout << setw(20);
	cout << "Vehicle type:";
	cout << setw(20);
	cout << "Pets allowed:";
	cout << setw(20);
	cout << "Number of passengers:";
	cout << setw(20);
	cout << "Ride time:";
	cout << setw(20);
	cout << "Status:";
	cout << endl;
	ride *rideptr;
	int i;
	for(auto it = ridelist.begin(); it!=ridelist.end(); ++it)//iterate through vector
	{
		rideptr = *it;
		cout << left;
		cout << setw(20);
		cout << rideptr->GetID();
		cout << setw(20);
		cout << rideptr->GetPassenger();
		cout << setw(20);
		cout << rideptr->GetDriver();
		cout << setw(20);
		rideptr->Getvehicle();
		cout << setw(20);
		rideptr->GetPets();
		cout << setw(20);
		rideptr->GetNumPassengers();
		cout << setw(20);
		rideptr->GetPickTime();
		cout << setw(20);
		rideptr->GetStatus();
		cout << endl;
	}
}

void rides::storerides()
{
	ofstream ridesout;
	ridesout.open("ridess.data");
	ridesout << rcounter << endl;
	ride *rideholder;
	for(auto it=ridelist.begin(); it!=ridelist.end(); ++it)
	{
		rideholder = *it;
		ridesout << rideholder->GetID() << "\t" << rideholder->GetDriver() << "\t" << rideholder->GetPassenger() << "\t"<< rideholder->GetDate() << "\t" << rideholder->GetMonth() << "\t";
		ridesout << rideholder->GetYear()<< "\t" << rideholder->GetMins() << "\t" << rideholder->GetHrs() << "\t" << rideholder->GetStatus() <<"\t" << rideholder->GetDropLoc() << "\t";
		ridesout << rideholder->GetPickLoc() << "\t" << rideholder->GetNumPassengers() << "\t";
		ridesout << rideholder->GetPets() << "\t" << rideholder->Getvehicle() << endl;
	}//store ride info into file
	ridesout.close();
	
}
void rides::loadrides()
{
	string driverID, passengerID, rideID; 
	int date, month, year, picktimemins, picktimehrs, droptimemins, droptimehrs, num, pick, i;
	char statusofride, petsallowed, cartype;
	string droploc, pickloc;
	float rating;
	bool pets;
	ifstream ridesin;
	ridesin.open("ridess.data");
	ridesin >> rcounter;
	ridesin.ignore();

	for(int i=0; i<rcounter; i++)
	{	
		ridesin >> rideID >> driverID >> passengerID >> date >> month >> year >> picktimemins >> picktimehrs >> statusofride >> droploc >> pickloc >> num >> pets >> cartype;//read in values
		ridelist.push_back(new ride(rideID, driverID, passengerID, cartype, date, month, year, picktimemins, picktimehrs, statusofride, droploc, pickloc, num, pets, rating));//insert ride into vector
	}
	ridesin.close();
}

void rides::clean()
{
	for(auto it = ridelist.begin(); it != ridelist.end(); ++it)
	{
		delete *it;//delete entity
	}
	ridelist.clear();
}
