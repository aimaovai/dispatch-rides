#include <iostream>
#include <string>
#include "driver.h"
#include "basic.h"

using namespace std;

basic::basic()
{
	seats = 4;
	cargo = 3;
}

basic::basic (int numseats, int numcargo, string name, string id, float rate, char avail, char pet, bool hand, string note):driver(name,id,rate,avail,pet,hand,note)
{
	seats = numseats;
	cargo = numcargo;
}

int basic::GetSeats()
{
	return seats;
}

int basic::GetCargo()
{
	return cargo;
}

void basic::PrintInfo()
{
	driver::PrintInfo();
	cout << "Ride type: Basic" << endl;
	cout << "Max number of passengers: " << seats << endl;
	cout << "Max number of luggage: " << cargo << endl;

}
