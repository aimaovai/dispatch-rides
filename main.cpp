/*Name: Aima Ovai
 *Email: aimaovai@my.unt.edu
 *Program: 
 */

#include <iostream>
#include <string>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include "driver.h"
#include "drivers2.h"
#include "passenger.h"
#include "passengers.h"
#include "ride.h"
#include "rides.h"
#include "basic.h"
#include "economy.h"
#include "group.h"
#include "luxury.h"

using namespace std;
void printheader();
passengers riders;
drivers ridedrivers;
rides dispatch;

int main()
{
	system("clear");
	printheader();
	int choice;
	string id;
	char option, status;
	riders.loadpassenger();
	ridedrivers.loaddrivers();
	dispatch.loadrides();
	printf("Welcome to the EagleLift Application.\nThis program has been designed to help you \nschedule rides to passengers and drivers");
	cout << endl;

	while(choice!=0)
	{
		cout << "Menu" << endl;
		printf("1. Add ride\n2. Add passenger\n3. Add driver\n4. Print rides\n5. Print passengers\n6. Print drivers\n7. Edit entries\n8. Delete entries\n0. quit\n");
		cin >> choice;
		cin.ignore();
		while((choice!=0)&&(choice!=1)&&(choice!=2)&&(choice!=3)&&(choice!=4)&&(choice!=5)&&(choice!=6)&&(choice!=7)&&(choice!=8))
		{
			cout << "Please enter a valid choice number: ";
			cin >> choice;
			cin.ignore();
		}

		switch(choice)
		{
			case 1: 
				system("clear");
				cout << "ADDING NEW RIDE" << endl;
				cout << endl;
				dispatch.addride();
				break;

			case 2:
				system("clear");
				cout << "ADDING PASSENGER" << endl;
				cout << endl;
				riders.addpassenger();
				break;

			case 3:
				system("clear");
				cout << "ADDING DRIVER" << endl;
				cout << endl;
				ridedrivers.adddriver();
				break;

			case 4:
				system("clear");
				cout << "PRINTING RIDES" << endl;
				cout << endl;
				dispatch.printallrides();
				break;

			case 5:
				system("clear");
				cout << "PRINTING PASSENGERS" << endl;
				cout << endl;
				riders.printallpassengers();
				break;

			case 6:
				system("clear");
				cout << "PRINTING DRIVERS" << endl;
				cout << endl;
				ridedrivers.printalldrivers();
				break;

			case 7:
				system("clear");
				cout << "EDITING ENTRY" << endl;
				cout << endl;
				cout << "d. Edit driver\np. Edit passenger\nr. Edit ride\nq. Return to main menu" << endl;
				cin >> option;
				cin.ignore();
				while((option!='d')&&(option!='p')&&(option!='r')&&(option!='q'))
				{
					cout << "Please make a valid selection. e, p, or r: " << endl;
					cin >> option;
					cin.ignore();
					cout << endl;
				}
					if(option=='d')
					{
						cout << "Enter the ID of driver: ";
						cin >> id;
						cin.ignore();
						ridedrivers.editdriver(id);
					}

					if(option=='p')
					{	
						cout << "Enter the ID of passenger: ";
						cin >> id;
						cin.ignore();
						riders.editpassenger(id);
					}

					if(option=='r')
					{
						cout << "Enter the ID of ride: ";
						cin >> id;
						cin.ignore();
						dispatch.editride(id);
					}

				break;

			case 8:
				system("clear");
				cout << "DELETING ENTRY" << endl;
				cout << endl;
				cout << "d. Delete driver\np. Delete passenger\nr. Delete ride\ns. Delete ride by status" << endl;
				cin >> option;
				cin.ignore();
				while((option!='d')&&(option!='p')&&(option!='r'))
				{
					cout << "Please make a valid selection. d, p, r, or q: " << endl;
					cin >> option;
					cin.ignore();
					cout << endl;
				}

				switch(option)
				{
					case 'd':
						cout << "Enter the ID of driver";
						cin >> id;
						cin.ignore();
						ridedrivers.deletedriverbyID(id);
						break;

					case 'p':
						cout << "Enter the ID of passenger";
						cin >> id;
						cin.ignore();
						riders.deletepassengerbyID(id);
						break;

					case 'r':
						cout << "Enter the ID of ride";
						cin >> id;
						cin.ignore();
						dispatch.deleteride(id);
						break;
					case 's':
						cout << "Enter status to delete by (C for complete, c for cancelled): " << endl;
						cin >> status;
						cin.ignore();
						dispatch.deleteride(status);
						break;

					defualt:
						break;
				}
				
				break;
			case 9:
				system("clear");
				cout << "OTHER PRINT OPTIONS" << endl;
				cout << "a. Print rides by driver\nb. Print rides by status\nc. Print rides by passenger" << endl;
				while((option!='a')&&(option!='b')&&(option!='c'))
				{
					cout << "Please enter a valid selection: " << endl;
					cin >> option;
					cin.ignore();
				}
				if(option=='a')
				{
					cout << "Enter driver's ID: " << endl;
					cin >> id;
					cin.ignore();
					dispatch.findridebydriver(id);
				}
				else if(option=='b')
				{
					cout << "Enter status type (A for active, C for complete, D for cancelled): ";
					cin >> status;
					cin.ignore();
					while((status!='A')&&(status!='C')&&(status!='D'))
					{
						cout << "Please make a valid selection: " << endl;
						cin >> status;
						cin.ignore();
					}
					dispatch.findridebystatus(status);
				}
				else
				{
					cout << "Enter passenger's ID: " << endl;
					cin >> id;
					cin.ignore();
					dispatch.findridebypassenger(id);
				}
				break;

			default:
				break;
		}

	}

	system("clear");
	riders.storepassenger();
	riders.clearspace();
	ridedrivers.storedrivers();
	ridedrivers.clear();
	system("clear");
	dispatch.storerides();
	dispatch.clean();
	system("clear");
	cout << endl;
	cout << "Program exited" << endl;


	return 0;	
}

void printheader()
{
	cout << endl;
	cout << "*************************************************************" << endl;
	cout << "*       Department: Computer Science and Engineering        *" << endl;
	cout << "*              Course Number: CSCE 1040                     *" << endl;
	cout << "*                   Name: Aima Ovai                         *" << endl;
	cout << "*               Email:aimaovai@my.unt.edu                   *" << endl;
	cout << "*************************************************************" << endl;
	cout << endl;
}
