#ifndef PASSENGERS_H
#define PASSENGERS_H

#include <iostream>
#include <string>
#include <vector>
#include "passenger.h"
#include "ride.h"

#define CHUNKSIZE 3
using namespace std;

class passengers
{
	private:
		int counter;
		int maxcount;
		typedef vector<passenger*>passengertype;
		passengertype passengerlist;
	public:
		passengers(){counter = 0;}
		int getcounter(){return counter;}		
		~passengers(){}
		void addpassenger();
		void editpassenger(string id);
		void deletepassengerbyID(string ID);
		int findpassengerbyname(string name);
		passenger* findpassenger(string id);
		string getpassenger(string id);
		int findpassengerbyID(string Id);
		bool findpet(string id, int index);
		bool findhandi(string id, int index);
		bool findnumpass(string pass);
		float findrating(string id, int index);
		void printfoundpassengerbyID(string ID, passengers *pptr1);
		void printallpassengers();
		void clearspace();
		void storepassenger();
		void loadpassenger();
};
#endif

