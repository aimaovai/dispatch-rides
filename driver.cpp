#include <iostream>
#include <string>
#include "driver.h"

using namespace std;

driver::driver()
{
	drivername = "none";
	driverID = "none";
	rating = 0.00;
	available = false;
	pets = false;
	handicap = false;
	drivernotes = "none";
}

driver::driver(string name, string id, float rate, bool avail, bool pet, bool hand, string note)
{
	drivername = name;
	driverID = id;
	rating = rate;
	available = avail;
	pets = pet;
	handicap = hand;
	drivernotes =  note;
}

void driver::SetDriverName(string name)
{
	drivername = name;
}

string driver::GetDriverName()
{
	return drivername;
}

void driver::SetDriverID(string id)
{
	driverID = id;
}

string driver::GetDriverID()
{
	return driverID;
}

void driver::SetAvailability(bool avail)
{
	available = avail;
}

bool driver::GetAvailability()
{
	if(available==true)
	{
		cout << "Available for ride";
	}
	else
	{
		cout << "Not available for rides";
	}
	return available;
}

void driver::SetDriverRating(float rating)
{
	this->rating = rating;
}

float driver::GetDriverRating()
{
	return rating;
}

void driver::SetPets(bool pet)
{
	pets = pet;
}

bool driver::GetPets()
{
	if(pets==true)
	{
		cout << "Allows pets";
	}
	else
	{
		cout << "Does not allow pets";
	}
	return pets;
}

void driver::SetDriverNotes(string notes)
{
	drivernotes = notes;
}

void driver::SetHandi(bool handi)
{
	handicap = handi;
}

bool driver::GetHandi()
{
	if(handicap==true)
	{
		cout << "Has handicap benefits";
	}
	else
	{
		cout << "No handicap benefits";
	}
	return handicap;
}

string driver::GetDriverNotes()
{
	return drivernotes;
}

void driver::PrintInfo()
{
	cout << "Name: " << drivername << endl;
	cout << "Driver ID: " << driverID << endl;
	cout << "Rating: " << rating << endl;
	cout << "Availability: " << available << endl;
	cout << "Pets: " << pets << endl;
	cout << "Handicap availability: " << handicap << endl;
	cout << "Notes: " << drivernotes << endl;
	
}
