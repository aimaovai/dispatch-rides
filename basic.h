#ifndef BASIC_H
#define BASIC_H

#include <iostream>
#include <string>
#include "driver.h"

using namespace std;

class basic : public driver
{
	protected:
		int seats;//2-4 passengers
		int cargo;
	public:
		basic();
		~basic();
		basic(int numseats, int numcargo, string name, string id, float rate, char avail, char pet, bool hand, string note);
		int GetSeats();
		int GetCargo();
		void PrintInfo();//virtual function
};

#endif
