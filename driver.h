#ifndef DRIVER_H
#define DRIVER_H

#include <iostream>
#include <string>

using namespace std; 

class driver
{
	private:
		string drivername;
		string driverID;
		float rating;
		bool available;
		bool pets;
		bool handicap;
		string drivernotes;
		
	public:
		driver();
		~driver(){};
		driver(string name, string id, float rate, bool avail, bool pet, bool hand, string note);
		void SetDriverName(string name);
		string GetDriverName();
		void SetDriverID(string id);
		string GetDriverID();
		void SetDriverRating(float rating);
		float GetDriverRating();
		void SetAvailability(bool avail);
		bool GetAvailability();
		void SetPets(bool pet);
		bool GetPets();
		void SetHandi(bool handi);
		bool GetHandi();
		void SetDriverNotes(string notes);
		string GetDriverNotes();
		virtual void PrintInfo();//virtual function




};

#endif
