#include <iostream>
#include <string.h>
#include <vector>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <iomanip>
#include "economy.h"
#include "basic.h"
#include "group.h"
#include "luxury.h"
#include "driver.h"
#include "drivers2.h"


using namespace std;

void drivers::adddriver()
{
	string ID, note, lnote;
	size_t dec;
	int capacity, pick, cargo;
	string name;
	float rating;
	char handi, vehicle, pet, avai;
	bool handicap, pets, available, sameid=true;
	driver *hold;
	luxury *lhold;
	int i=0;
	
		rating = 0.00;
		cout << "Enter name of driver: ";
		getline(cin, name);
//		cin.ignore();
		cout << "Enter unique 6 digit driver ID: ";
		getline(cin, ID);
		cin.ignore();

//		while(sameid)
//		{
			for(int a=0; a<driverlist.size(); a++)
			{
				if(driverlist.at(a)->GetDriverID()!=ID)//check if entered ID already belongs to previous passengeir
				{
					sameid = false;
					break;
				}
			
				else
				{
					cout << "Passenger ID already exists. Please enter a new ID or press 1 to exit to main menu: " << endl;
					getline(cin, ID);
					cin.ignore();
					if(ID=="1")//cehck if user wants to exit
					{
						system("clear");
						break;
					}							
				}
			
			}
		
//		}

		cout << "Are you available to receive or accept rides now and in the future? Y/y for yes or N/n for no: ";
		cin >> avai;
		cin.ignore();
		while((avai!='Y')&&(avai!='y')&&(avai!='N')&&(avai!='n'))
		{
			cout << "Please enter a valid option. Y/y for yes, N/n for no: ";
			cin >> avai;
			cin.ignore();
		}
		if((avai=='Y')||(avai=='y'))
		{
			available = true;
		}
		else
		{
			available = false;
		}
		cout << "Would you have handicap provisions? Y/y for yes or N/n for no: ";
		cin >> handi;
		cin.ignore();
		while((handi!='Y')&&(handi!='y')&&(handi!='N')&&(handi!='n'))
		{
			cout << endl;
			cout << "Please enter a valid option. Y/y for yes, N/n for no: ";
			cin >> handi;
			cin.ignore();
		}
		if((handi=='Y')||(handi=='y'))
		{
			handicap = true;
		}
		else
		{
			handicap = false;
		}
		cout << "Would you allow pets?: ";
		cin >> pet;
		cin.ignore();
		while((pet!='Y')&&(pet!='y')&&(pet!='N')&&(pet!='n'))
		{
			cout << endl;
			cout << "Please enter a valid selection. Y/y for yes or N/n for no: ";
			cin >> pet;
			cin.ignore();
		}
		if((pet=='Y')||(pet=='y'))
		{
			pets = true;
		}
		else
		{
			pets = false;
		}
		cout << "Enter any special notes about driver: " << endl;
		getline(cin, note);
		cout << "Choose a car type:\n1. Economy: 1-2 passengers, 1 bags max\n2. Basic: 2-4 passengers, 4 bags max\n3. Group: 5-7 passengers, 10 bags max\n4. Luxury: Has different features" << endl;
		cin >> pick;
		cin.ignore();
		while((pick!=1)&&(pick!=2)&&(pick!=3)&&(pick!=4))
		{
			cout << "Please enter a valid selection: " << endl;
			cin >> pick;
			cin.ignore();
		}

		switch(pick)
		{
			case 1:
				capacity = 2;
				cargo = 1;
				hold = new economy(capacity, cargo, name, ID, rating, available, pets, handicap, note);
				driverlist.push_back(hold);
				count++;
				break;
			case 2:
				capacity = 4;
				cargo = 4;
				hold = new basic(capacity, cargo, name, ID, rating, available, pets, handicap, note);
				driverlist.push_back(hold);
				count++;
				break;
			case 3:
				capacity = 7;
				cargo = 10;
				hold = new group(capacity, cargo, name, ID, rating, available, pets, handicap, note);
				driverlist.push_back(hold);
				count++;
				break;
			case 4:
				cout << "Enter details of luxury vehicle: " << endl;
				cout << "Passenger fit: ";
				cin >> capacity;
				cin.ignore();
				cout << "Amount of bags: ";
				cin >> cargo;
				cin.ignore();
				cout << "Other vehicle features: ";
				getline(cin, lnote);
				hold = new luxury(capacity, cargo, lnote, name, ID, rating, available, pets, handicap, note);
				driverlist.push_back(hold);
				count++;
			//	lhold->SetNotes(lnote);
				break;
			default:
				break;
		}
		//hold = new driver(name, ID, rating, available, pets, handicap, note);
		//driverlist.push_back(hold);
		//count++;
	system("clear");
}

void drivers::editdriver(string id)
{
	driver *edit;
	char hand, pet, vehicle;
	string name, quit, driverid;
	int select, nID, cap;
	bool notfound = true, same = true, setpet, sethandi;
//	while(driverid!="0")
//	{
	/*	while(driverid=="0")
		{
			break;
		}*/
		while(notfound)
		{
			for(int i=0; i<driverlist.size(); i++)
			{
				if(driverlist.at(i)->GetDriverID()==id)//check if user exists in list o passengers
				{
					notfound = false;
					
					cout << "Driver found." << endl;
					cout << endl;
					printalldrivers();
					cout << endl;
					cout << "What information would you the driver like to change?\n1. Name\n2. Handicap requirements\n3. Pet restrictions\nPlease select 1, 2, or 3, or press 0 to return to the main menu." << endl;
					cin >> select;
					cin.ignore();
					while((select!=1)&&(select!=2)&&(select!=3)&&(select!=4)&&(select!=5)&&(select!=0))//check for choice validity
					{
						cout << "Please select either 1, 2, or 3." << endl;
						cin >> select;
						cin.ignore();
					}
					switch(select)
					{
						case 0:
							system("clear");
							break;
						case 1:
							cout << "Enter new name: " << endl;
							cin >> name;
							cin.ignore();
							driverlist.at(i)->SetDriverName(name);//set passenger name to new name
							system("clear");
							break;
		
						case 2:
							cout << "Enter handicap requirement (Y/y for yes or N/n for no): " << endl;
							cin >> hand;
							cin.ignore();
							while((hand!='Y')&&(hand!='y')&&(hand!='N')&&(hand!='n'))//check for answer validity
							{
								cout << "Please enter Y/y or N/n." << endl;
								cin >> hand;
								cin.ignore();
							}
							if((hand=='y')||(hand=='Y'))
							{
								sethandi = true;
							}
							else
							{
								sethandi = false;
							}
							driverlist.at(i)->SetHandi(sethandi);//set handicap requirement to new option
							system("clear");
							break;
						case 3:
							cout << "Enter new pet restriction. (Y/y for yes, or N/n for no): " << endl;
							cin >> pet;
							cin.ignore();
							while((pet!='Y')&&(pet!='y')&&(pet!='N')&&(pet!='n'))//check for answer validity
							{
								cout << "Please enter Y/y or N/n." << endl;
								cin >> pet;
								cin.ignore();
							}
							if((pet=='y')||(pet=='Y'))
							{
								setpet = true;
							}
							else
							{
								setpet = false;
							}
							driverlist.at(i)->SetPets(setpet);//set pet choice to new choice
							system("clear");
							break;
							
						default:
							break;
					}
				}
			}
		}
			if(notfound)
			{
				string key;
				cout << "Passenger not found. Press any key to return to main menu." << endl;
				cin >> key;
				system("clear");
			}
	
	
}


void drivers::deletedriverbyID(string ID)
{
	for(int i=0; i<driverlist.size(); i++)
	{
		if(driverlist.at(i)->GetDriverID()==ID)
		{
			delete driverlist[i];
			driverlist.erase(driverlist.begin()+i);
			count = driverlist.size();
		}
	}
	return;
}



driver* drivers::findbyID(string id)
{
	for (int i =0; i < count; i++)
	{
        if (driverlist[i]->GetDriverID() == id) 
		{
			return driverlist[i];
		}
    }
  
    return NULL;
}

bool drivers::findbyid(string id)
{
	for(int i=0; i<count; i++)
	{
		if(driverlist[i]->GetDriverID()==id)
		{
			return true;
		}
	}
}

string drivers::findbypoint(int point)
{
			return driverlist.at(point)->GetDriverID();//return the id of driver at given point
}


bool drivers::checkdriveravai(int index)
{
		if(driverlist.at(index)->GetAvailability()==true)
		{
			return true;
		}
		else
		{
			return false;
		}
}

bool drivers::checkdriverrating(int index, float rate)
{		
		if((abs(driverlist.at(index)->GetDriverRating()-rate)<0.0001))
		{	
			return true;
		}
		else
		{
			return false;
		}
}

bool drivers::GetPetofdriver(int index, bool pet)
{
	return driverlist.at(index)->GetPets();
}

string drivers::getdriver(int point)
{
	return driverlist.at(point)->GetDriverID();
}

void drivers::printdriver(int index)
{
	driver *printptr;
/*	cout << "Drivers:" << endl;
	cout << left;
	cout << setw(25);
	cout << "Name:";
	cout << setw(20);
	cout << "ID:";
	cout << setw(20);
	cout << "Availability:";
	cout << setw(20);
	cout << "Handicap:";
	cout << setw(20);
	cout << "Pets:";
	cout << setw(20);
	cout << "Driver notes:";
	cout << setw(20);
	cout << "Number of seats:";
	cout << setw(20);
	cout << "Amount of luggage:";
	cout << setw(20);
	cout << "Def. rating: ";
	cout << endl;
	
	cout << left;
	cout << setw(25);
	cout << driverlist.at(index)->GetDriverName();
	cout << setw(20);
	cout << driverlist.at(index)->GetDriverID();
	cout << setw(20);
	driverlist.at(index)->GetAvailability();
	cout << setw(20);
	driverlist.at(index)->GetHandi();
	cout << setw(20);
	driverlist.at(index)->GetPets();
	cout << setw(20);
	cout << driverlist.at(index)->GetDriverNotes();
	cout << setw(20);
	cout << driverlist.at(index)->GetSeats();
	cout << setw(20);
	cout << driverlist.at(index)->GetCargo();
	cout << setw(20);
	cout << driverlist.at(index)->GetDriverRating();
	cout << endl;
	cout << endl;
	*/
	driverlist.at(index)->PrintInfo();
}


bool drivers::gethandistatus(int index, bool handi)
{
  		if(driverlist.at(index)->GetHandi() == handi)
		{
			return true;
		}
		else
		{
		    	return false;
		}
}

void drivers::printalldrivers()
{

	driver *dptr;
	for(auto it = driverlist.begin(); it!=driverlist.end(); ++it)
	{
		dptr = *it;	
		dptr->PrintInfo();
		cout << endl;
		cout << endl;
	}
}

void drivers::storedrivers()
{
	ofstream driverout;
	driverout.open("drivers.data");
	count = driverlist.size();
	driverout << count << endl;
	driver *newhold;
	for(auto it=driverlist.begin(); it!=driverlist.end(); ++it)
	{
		newhold = *it;
		driverout << newhold->GetDriverName() << ",\t";
		driverout << newhold->GetDriverID() << "\t";
		driverout << newhold->GetAvailability() << "\t";
		driverout << newhold->GetHandi() << "\t";
		driverout << newhold->GetPets() << "\t";
		driverout << newhold->GetDriverRating() << "\t";
//		driverout << newhold->GetDriverNotes() << "," << " ";
		if(typeid(**it)==typeid(economy))
		{
			economy *eptr = dynamic_cast<economy*>(newhold);
			driverout << "1\t" << eptr->GetSeats() << "\t" << eptr->GetCargo() << "\t" << newhold->GetDriverNotes() << endl;
		}
		else if(typeid(**it)==typeid(basic))
		{
			basic *bptr = dynamic_cast<basic*>(newhold);
			driverout << "2\t" << bptr->GetSeats() << "\t" << bptr->GetCargo() << "\t" << newhold->GetDriverNotes() << endl;
		}
		else if(typeid(**it)==typeid(group))
		{
			group * gptr = dynamic_cast<group*>(newhold);
			driverout << "3\t" << gptr->GetSeats() << "\t" << gptr->GetCargo() << "\t" << newhold->GetDriverNotes() << endl;
		}
		else if(typeid(**it)==typeid(luxury))
		{
			luxury *lptr = dynamic_cast<luxury*>(newhold);
			driverout << "4\t" << lptr->GetSeats() << "\t" << lptr->GetCargo() << "\t" << newhold->GetDriverNotes() << "," << lptr->GetNotes() << endl;
		}

	}
	driverout.close();
	system("clear");
}

void drivers::loaddrivers()
{
	int capacity, cargo, vehicle;
	string name, note, lnote, ID, skip;
	float rating;
	char handi, pet, avai;
	bool handicap, pets, available;
	
	ifstream driverin;
	driverin.open("drivers.data");
	driverin >> count;
	driverin.ignore();
	luxury *ptr;
	basic *bptr;
	economy *eptr;
	group *gptr;
	for(int i=0; i<count; i++)
	{	
		getline(driverin, name, ',');
		driverin >> ID >> available >> handicap >> pets >> rating >> vehicle; 
	//	getline(driverin, note, ',');
		//	getline(getline(driverin, skip, '"'), note, '"');
	//	scanf("%s", note);
	//	driverin >> vehicle;
	//	getline(getline(driverin, skip, '"'), lnote, '"');
	       //	getline(driverin, lnote);
		if(vehicle==1)
		{
			driverin >> capacity >> cargo;
			getline(driverin, note);
			eptr = new economy(capacity, cargo, name, ID, rating, available, pets, handicap, note);
			driverlist.push_back(eptr);
		}
		else if(vehicle==2)
		{
			driverin >> capacity >> cargo;
			getline(driverin, note);
			bptr = new basic(capacity, cargo, name, ID, rating, available, pets, handicap, note);
			driverlist.push_back(bptr);
		}
		else if(vehicle==3)
		{
			driverin >> capacity >> cargo;
			getline(driverin, note);
			gptr = new group(capacity, cargo, name, ID, rating, available, pets, handicap, note);
			driverlist.push_back(gptr);
		}
		else if(vehicle==4)
		{
			driverin >> capacity >> cargo;
			getline(driverin, note, ',');
			getline(driverin, lnote);
			ptr = new luxury(capacity, cargo, lnote, name, ID, rating, available, pets, handicap, note);	
			driverlist.push_back(ptr);
		//	ptr->SetNotes(lnote);
		}

	//	driverlist.push_back(new driver(name, driverID, rating, available, pets, handicap, notes));	
	}
	driverin.close();
	
}

void drivers::clear()
{
	for(auto it = driverlist.begin(); it != driverlist.end(); ++it)
	{
		delete *it;
	}
	driverlist.clear();
}
