#ifndef RIDE_H
#define RIDE_H

#include <iostream>
#include <string>
using namespace std;

class ride
{
	private:
		string rideID;
		int date;
		int month;
		int year;
		int picktimemins;
		int picktimehrs;
		int droptimemins;
		int droptimehrs;
		char statusofride;
		string driver;
		char vehicle;
		string passenger;
		string droplocation;
		string picklocation;
		int numpass;
		bool petsallowed;
		enum ridestatus{complete = 'C', active = 'A', Cancelled = 'c'};
		float custrating;
	public:
		ride();
		~ride(){}
		ride (string ID, string driver, string passenger, char car, int date, int month, int year, int picktimemins, int picktimehrs, char statusofride, string droplocation, string picklocation, int numpass, bool petsallowed, float custrating);
		ride (string ID, string driver, string passenger, char car, int date, int month, int year, int picktimemins, int picktimehrs, int droptimehrs, int droptimemins, char statusofride, string droplocation, string picklocation, int numpass, bool petsallowed, float custrating);
		void SetID(string rideid);
		string GetID();
		void SetDriver(string drivertoset);
		string GetDriver();
		void SetPassenger(string passengertoset);
		string GetPassenger();
		void SetPickLoc(string location);
		string GetPickLoc();
		void SetPickTime(int ptimehrs, int ptimemins, int ptimedate, int ptimemonth, int ptimeyr);
		int GetPickTime();
		void SetDropLoc(string location);
		string GetDropLoc();
		void SetNumPassengers(int numpass);
		int GetNumPassengers();
		void Setvehicle(char vehicle);
		char Getvehicle();
		void SetPets(bool pets);
		bool GetPets();
		void SetDropTime(int dtimehrs, int dtimemins);
		int GetDropTime();
		int GetMins();
		int GetHrs();
		int GetDate();
		int GetMonth();
		int GetYear();
		void SetStatus(char stats);
		char GetStatus();
		void SetCustRating(float rate);//rating set by user after ride
		float GetCustRating();
};

#endif
