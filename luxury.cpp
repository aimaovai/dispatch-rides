#include <iostream>
#include <string>
#include "driver.h"
#include "luxury.h"

using namespace std;

luxury::luxury()
{
	seatamount = 0;
	cargoamount = 0;
	notes = "none";
}

luxury::luxury(int seat, int cargo, string notes, string name, string id, float rate, char avail, char pet, bool hand, string note):driver(name,id,rate,avail,pet,hand,note)
{
	seatamount = seat;
	cargoamount = cargo;
	this->notes = notes;
}

int luxury::GetCargo()
{
	return cargoamount;
}

int luxury::GetSeats()
{
	return seatamount;
}

void luxury::SetNotes(string note)
{
	notes = note;
}

string luxury::GetNotes()
{
	return notes;
}

void luxury::PrintInfo()
{
	driver::PrintInfo();
	cout << "Ride type: Luxury" << endl;
	cout << "Amount of passengers: " << seatamount << endl;
	cout << "Amount of cargo: " << cargoamount << endl;
	cout << "Notes about driver: " << notes << endl;
	cout << endl;
}
